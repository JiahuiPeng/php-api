<?php
$dbhost = 'localhost';  // mysql服务器主机地址
$dbuser = '';            // mysql用户名
$dbpass = '';          // mysql用户名密码
$dbname = '';
$dbport = ;
$db = mysqli_connect($dbhost, $dbuser, $dbpass, $dbname, $dbport);

define('__PLUGIN_ROOT__', dirname(__FILE__));
define('__UPLOAD_FOLDER__',__PLUGIN_ROOT__ . "/upload");

if ( ! defined( 'ABSPATH' ) ) {
	exit; // Exit if accessed directly.
}

if ( !class_exists( 'myfunction' ) ) {
    class myfunction
    {   
        function __construct(){

        }
		
		static function test_login(){
            $response["code"] = "200";
			$response["message"] = "login seccessfully!";
			$response["data"] = ['username' => ''];
			return $response;
        }

        static function test_post_p($r){
            //$parameters = $r->get_body_params();
            $parameters = $r->get_params();
            $username = $parameters['username'];
            $password = $parameters['password'];
            //database operation.....
            $response["code"] = "200";
            $response["message"] = "seccessfully!";
            $response["data"] = ['username' => $username, 'password' => $password];
            return $response;
        }
        // login module
        /*  
            login function:
            If database server is fine and input data is right,
            return staff_id, staff_name and staff_authority.
        */ 
        static function login($request_data){
            $parameters = $request_data->get_params();
            $staff_account = $parameters['staff_account'];
            $staff_password = $parameters['staff_password'];
            global $db;
            if (!$db) {
                $response['code'] = "500";
                $response['message'] = "database error! ".mysqli_connect_error();
                $response['data'] = $_empty;
                mysqli_close($db);
                return $response; 
            }
            $sql = "SELECT staff_id, staff_name, staff_authority, staff_photo FROM crm_staffs WHERE staff_account='$staff_account' AND staff_password='$staff_password'";
            global $wpdb;
            $row = $wpdb->get_row($sql, ARRAY_A);
            if (!$row){
                $response['code'] = "400";
                $response['message'] = "Account or password wrong. ";
                $response['data'] = $_empty;
                mysqli_close($db);
                return $response;
            }else{
                $response["code"] = "200";
                $response["message"] = "login seccessfully, return staff detail!";
                $response["data"] = ['staff_id' => $row['staff_id'], 'staff_name' => $row['staff_name'], 
                'staff_authority' => $row['staff_authority'], 'staff_photo' => $row['staff_photo']];
                mysqli_close($db);
                return $response;
            }
        }

        // customer module
        /* 
            customer function:
            offers staff id then represents the relevant customers list.
            customer_authority : 1 => private customers, only match the right staff_id with customer_staff 
            customer_authority : 2 => public customers, whose every staff can check. 
        */
        static function customers($request_data){
            $parameters = $request_data->get_params();
            $staff_id = $parameters['staff_id'];
            $staff_authority = $parameters['staff_authority'];
            global $db;
            if (!$db) {
                $response['code'] = "500";
                $response['message'] = "database error! ".mysqli_connect_error();
                $response['data'] = $_empty;
                mysqli_close($db);
                return $response; 
            }
            $sql="";
            if($staff_authority == 2){    //管理员sql
                $sql = "SELECT customer_id, customer_name, customer_company, customer_position, customer_photo FROM crm_customers";
            }else{                        //普通员工sql
                $sql = "SELECT customer_id, customer_name, customer_company, customer_position, customer_photo FROM crm_customers WHERE customer_staff= '$staff_id' ";
            }
            mysqli_query($db,'set names utf8');
            $result = mysqli_query($db, $sql);
            if (mysqli_num_rows($result)!=0)
            {
                $response["code"] = "200";
                $response["message"] = "Customers details will be demonstrated! ";
                $response['data']=[];
                while ($row=mysqli_fetch_row($result)){
                    $_temp = array('customer_id' => $row[0], 'customer_name' => $row[1], 'customer_company' => $row[2], 'customer_position' => $row[3], 'customer_photo' => $row[4]);
                    array_push($response['data'],$_temp);
                }
                mysqli_free_result($result);
                mysqli_close($db);
                return $response;
            }else{
                $response["code"] = "200";
                $response['message'] = "no results list...." ;
                $response['data']= $_empty;
                mysqli_free_result($result);
                mysqli_close($db);
                return $response;
            }
        }

        static function customer_details($request_data){
            $parameters = $request_data->get_params();
            $customer_id = $parameters['customer_id'];
            global $db;
            if (!$db) {
                $response['code'] = "500";
                $response['message'] = "database error! ".mysqli_connect_error();
                $response['data'] = $_empty;
                mysqli_close($db);
                return $response; 
            }
            $sql = "SELECT * FROM crm_customers WHERE customer_id= '$customer_id' ";
            mysqli_query($db,'set names utf8');
            $result = mysqli_query($db, $sql);
            $relevant_project = [];
            $response = [];
            if (mysqli_num_rows($result) == 0){
                $response["code"] = "400";
                $response["message"] = "Unexception error! Can't return detail ";
                $response['data']=[];
                mysqli_free_result($result);
                mysqli_close($db);
                return $response;
            }else{
                //search all related project names by project ids from table crm_customers_projects via customer_id
                global $wpdb;
                $sql_2 = "SELECT * FROM crm_customers_projects WHERE customer_id = '$customer_id' ";
                $result_rows = $wpdb->get_results($sql_2, ARRAY_A);
                foreach($result_rows as $row){
                    $pid=$row['project_id'];
                    $sql_3 = "SELECT project_name FROM crm_projects WHERE project_id = '$pid' ";
                    $pname = $wpdb->get_row($sql_3, ARRAY_A);
                    $_temp = array("project_id" => $pid, "project_name" => $pname['project_name']);
                    array_push($relevant_project, $_temp);
                }
                $response["code"] = "200";
                $response["message"] = "The customer's more details will be demonstrated! ";
                $row = mysqli_fetch_array($result);
                $response['data']=[
                    "customer_id" => $row["customer_id"],
                    "customer_name" => $row["customer_name"],
                    "customer_company" => $row["customer_company"],
                    "customer_position" => $row["customer_position"],
                    "customer_phone" =>  $row["customer_phone"],
                    "customer_gender" => $row["customer_gender"],
                    "customer_email" =>$row["customer_email"],
                    "customer_age" => $row["customer_age"],
                    "customer_address" => $row["customer_address"],
                    "customer_comment" => $row["customer_comment"],
                    "customer_photo" => $row["customer_photo"],
                    "customer_project" => $relevant_project    
                ];
                mysqli_free_result($result);
                mysqli_close($db);
                return $response;
            }
        }

        static function customer_insert($request_data){
            global $wpdb;
            $parameters = $request_data->get_params();
            $project_ids = [];
            if (array_key_exists("customer_project",$parameters)){
                $project_id_string = $parameters["customer_project"];
                unset($parameters["customer_project"]);
                //explode may cause error
                $project_ids = array_filter(explode(',',$project_id_string)); 
            }
            $result = $wpdb->insert('crm_customers',$parameters);
            if($result){
                $_t1=$parameters['customer_name'];
                $_t2=$parameters['customer_company'];
                $_t3=$parameters['customer_phone'];
                $_t4=$parameters['customer_gender'];
                $sql = "SELECT customer_id FROM crm_customers WHERE customer_name='$_t1' AND customer_company='$_t2' AND customer_phone ='$_t3' AND customer_gender='$_t4'";
                $_temp = $wpdb->get_row($sql, ARRAY_A);
                $customer_id = $_temp['customer_id'];
                if ($project_ids){
                    //insert table: crm_customer_project
                    //
                    foreach( $project_ids as $pid){
                        $_temp = array('customer_id' => $customer_id, 'project_id' => $pid);
                        $result_insert = $wpdb->insert('crm_customers_projects',$_temp);
                        if (!$result_insert){
                            $response['code'] = "400";
                            $response['message'] = "update table errors!";
                            $response['data'] = $_temp;
                            return $response;
                        }
                    }
                }
                if(myfunction::write_history($parameters['customer_staff'],$customer_id,"添加",$project_ids)){
                    $response['code'] = "200";
                    $response['message'] = "insert successful!";
                    $response['data'] = $_empty;
                    return $response;
                }else{
                    $response['code'] = "500";
                    $response['message'] = "can't update crm_history table";
                    $response['data'] = $_empty;
                    return $response;
                }
            }else{
                $response['code'] = "400";
                $response['message'] = "Excution SQL errors ".$wpdb->show_errors();
                $response['data'] = $parameters;
                return $response;
            }            
        }

        static function customer_update($request_data){
            $parameters = $request_data->get_params();
            $where['customer_id'] =  $parameters['customer_id'];
            unset($parameters['customer_id']);
            //$response=[];
            global $wpdb;
            $result = $wpdb->update('crm_customers',$parameters,$where);
            if($result){
                //update table: crm_customer_project
                if (array_key_exists("customer_project",$parameters)){
                    $cid = $where['customer_id'];
                    $project_id_string = $parameters["customer_project"];
                    unset($parameters["customer_project"]);
                    //explode function is wired sometime..... so use array_filter
                    $input_project_ids = array_filter(explode(',', $project_id_string));
                    $sql_project_ids =[];
                    $sql = "SELECT * FROM crm_customers_projects WHERE customer_id ='$cid' ";
                    $result_rows =  $wpdb->get_results($sql, ARRAY_A);                    
                    foreach($result_rows as $row){
                        array_push($sql_project_ids, $row['project_id']);                        
                    }
                    //在取得了输入的id值 和 数据库的id值后，我们开始比较。
                    //首先先找到交集元素 即位数据库中不改变的，
                    //输入的id值 set - 交集set =》新增的 projects 或者 求差集
                    //数据库的id set - 交集set =》应该删除的 projects 或者 求差集
                    foreach ($input_project_ids as $i) {
                        if ( ! in_array($i, $sql_project_ids)) {
                            $_temp = array('customer_id' => $cid, 'project_id' => $i);
                            $result_insert = $wpdb->insert('crm_customers_projects', $_temp);
                            $insert_pid[] = $i;
                        }                        
                    }                    
                    foreach ($sql_project_ids as $j) {
                        if ( ! in_array($j, $input_project_ids)) {
                            $_temp = array('customer_id' => $cid, 'project_id' => $j);
                            $result_delete = $wpdb->delete('crm_customers_projects', $_temp);
                            $delete_pid[] = $j;
                        }
                    }
                }
                $response['code'] = "200";
                $response['message'] = "Update items successful! ";
                $response['insert_pid'] = $insert_pid;
                $response['delete_pid'] = $delete_pid;
                return $response;
            }else{
                $response['code'] = "400";
                $response['message'] = "You may input empty or wrong data or no change ".$wpdb->show_errors();
                $response['data'] = $parameters;
                return $response;
            }
        }

        static function customer_delete($request_data){
            $parameters = $request_data->get_params();
            global $wpdb;
            $result = $wpdb->delete('crm_customers',$parameters);
            if($result){
                //delete crm_costomers_projects table as well
                $result2 = $wpdb->delete('crm_customers_projects',$parameters);
                if(!$result2){
                    $response['code'] = "400";
                    $response['message'] = "crm_costomers_projects table execution fail ".$wpdb->show_errors();
                    $response['data'] = $parameters;
                    return $response;
                }
                $response['code'] = "200";
                $response['message'] = "Delete items has completed!";
                $response['data'] = $_empty;
                return $response;

            }else{
                $response['code'] = "400";
                $response['message'] = "crm_costomers table execution fail ".$wpdb->show_errors();
                $response['data'] = $request_data->get_params();
                return $response;
            }
        }

        static function projects($request_data){
            $parameters = $request_data->get_params();
            $staff_id = $parameters['staff_id'];
            $staff_authority = $parameters['staff_authority'];
            global $wpdb;
            $sql = "";
            if($staff_authority == 2){                    //管理员 sql
                $sql = "SELECT * FROM crm_projects";
            }else{
                $sql = "SELECT * FROM crm_projects WHERE project_staff_id ='$staff_id'";     //普通员工 sql
            }
            $rows = $wpdb->get_results($sql, ARRAY_A );
            if($rows){
                $data = [];
                foreach($rows as $row){
                    $_temp = array('project_id'=>$row['project_id'], 'project_name'=>$row['project_name'], 'project_phase'=>$row['project_phase']);
                    array_push($data,$_temp);
                }
    
                $response["code"] = 200;
                $response["message"] =  "Projects details will be demonstrated!";
                $response["data"] = $data;
                return $response;
            }else{
                $response["code"] = 400;
                $response["message"] =  "Fail!";
                $response["data"] = $parameters;
                return $response;
            }
        }

        static function projects_details($request_data){
            $parameters = $request_data->get_params();
            $project_id = $parameters['project_id'];
            global $wpdb;
            $sql = "SELECT * FROM crm_projects WHERE project_id = '$project_id'";
            $row = $wpdb->get_row($sql, ARRAY_A);
            if($row){
                $relevant_customer = [];
                $sql_2 = "SELECT * FROM crm_customers_projects WHERE project_id = '$project_id' ";
                $result_rows = $wpdb->get_results($sql_2, ARRAY_A);               
                foreach($result_rows as $i){
                    $cid=$i['customer_id'];
                    $sql_3 = "SELECT * FROM crm_customers WHERE customer_id = '$cid' ";
                    $customer = $wpdb->get_row($sql_3, ARRAY_A);
                    if(!$result_rows){
                        $response["code"] = 400;
                        $response["message"] =  "Unexception error! Can't return detail!";
                        $response["data"] = $sql_3;
                        return $response;
                    }
                    $_temp = array("customer_id" => $cid, "customer_name" => $customer['project_name'], "customer_company" =>$customer['customer_company'],
                                    "customer_position" => $customer['customer_position'], "customer_photo" => $customer['customer_photo'], "customer_staff" => $customer['customer_staff']);
                    array_push($relevant_customer, $_temp);
                }
                $response["code"] = 200;
                $response["message"] =  "The project's more details will be demonstrated!";
                $response["data"] = [
                    "project_id" => $row['project_id'],
                    "project_name" => $row['project_name'],
                    "project_staff_name" => $row['project_staff_name'],
                    "project_staff_id" => $row['project_staff_id'],
                    "project_staff_email" => $row['project_staff_email'],
                    "project_staff_position" => $row['project_staff_position'],
                    "project_staff_phone" => $row['project_staff_phone'],
                    "project_staff_address" => $row['project_staff_address'],
                    "project_time" => $row['project_time'],
                    "project_description" => $row['project_description'],
                    "project_company_address" => $row['project_company_address'],
                    "project_phase" => $row['project_phase'],
                    "project_processing" => $row['project_processing'],
                    "project_authority" => $row['project_authority'],
                    "project_customer" => $relevant_customer
                ];
                return $response;
            }else{
                $response["code"] = 400;
                $response["message"] =  "Unexception error, Can't return detail!";
                $response["data"] = $parameters;
                return $response;
            }
        }

        static function project_insert($request_data){
            global $wpdb;
            $parameters = $request_data->get_params();
            
            $customer_ids = [];
            if (array_key_exists("project_customer",$parameters)){
                $customer_id_string = $parameters["project_customer"];
                unset($parameters["project_customer"]);
                //explode may cause error
                $customer_ids = array_filter(explode(',',$customer_id_string)); 
            }
            $result = $wpdb->insert('crm_projects',$parameters);
            if($result){
                if ($customer_ids){
                    //insert table: crm_customer_project
                    $_t1=$parameters['project_name'];
                    $_t2=$parameters['project_staff_name'];
                    $_t3=$parameters['project_staff_email'];
                    $_t4=$parameters['project_staff_position'];
                    $_t5=$parameters['project_staff_phone'];
                    $sql = "SELECT project_id FROM crm_projects WHERE project_name='$_t1' AND project_staff_name='$_t2' AND project_staff_email ='$_t3' AND project_staff_position='$_t4' 
                            AND project_staff_phone='$_t5' ";
                    $_temp = $wpdb->get_row($sql, ARRAY_A);
                    $project_id = $_temp['project_id'];
                    foreach( $customer_ids as $cid){
                        $_temp = array('customer_id' => $cid, 'project_id' => $project_id);
                        $result_insert = $wpdb->insert('crm_customers_projects',$_temp);
                        if (!$result_insert){
                            $response['code'] = "400";
                            $response['message'] = "update table errors!";
                            $response['data'] = $_temp;
                            return $response;
                        }
                    }
                }
                $response['code'] = "200";
                $response['message'] = "insert successful!";
                $response['data'] = $_empty;
                return $response;
            }else{
                $response['code'] = "400";
                $response['message'] = "Excution SQL errors ".$wpdb->show_errors();
                $response['data'] = $parameters;
                return $response;
            }
        }

        static function project_update($request_data){
            $parameters = $request_data->get_params();
            $where['project_id'] =  $parameters['project_id'];
            unset($parameters['project_id']);
            global $wpdb;
            $result = $wpdb->update('crm_projects',$parameters,$where);
            if($result){
                //update table: crm_customer_project
                $customer_id_string = [];
                if(array_key_exists("project_customer",$parameters)){
                    $customer_id_string = $parameters["project_customer"];
                    unset($parameters["project_customer"]);
                }
                $pid = $where['project_id'];
                //explode function is wired sometime..... so use array_filter
                $input_customer_ids = array_filter(explode(',', $customer_id_string));
                $sql_customer_ids =[];
                $sql = "SELECT * FROM crm_customers_projects WHERE project_id ='$pid' ";
                $result_rows =  $wpdb->get_results($sql, ARRAY_A);                    
                foreach($result_rows as $row){
                    array_push($sql_customer_ids, $row['customer_id']);                        
                }
                //在取得了输入的id值 和 数据库的id值后，我们开始比较。
                //首先先找到交集元素 即位数据库中不改变的，
                //输入的id值 set - 交集set =》新增的 projects 或者 求差集
                //数据库的id set - 交集set =》应该删除的 projects 或者 求差集
                foreach ($input_customer_ids as $i) {
                    if ( ! in_array($i, $sql_customer_ids)) {
                        $_temp = array('customer_id' => $i, 'project_id' => $pid);
                        $result_insert = $wpdb->insert('crm_customers_projects', $_temp);
                        $insert_pid[] = $i;
                    }                        
                }
                
                foreach ($sql_customer_ids as $j) {
                    if ( ! in_array($j, $input_customer_ids)) {
                        $_temp = array('customer_id' => $j, 'project_id' => $pid);
                        $result_delete = $wpdb->delete('crm_customers_projects', $_temp);
                        $delete_pid[] = $j;
                    }
                }

                $response['code'] = "200";
                $response['message'] = "Update items successful! ";
                $response['insert_cid'] = $insert_pid;
                $response['delete_cid'] = $delete_pid;
                return $response;
            }else{
                $response['code'] = "400";
                $response['message'] = "You may input empty or wrong data or no change ".$wpdb->show_errors();
                $response['data'] = $parameters;
                return $response;
            }
        }

        static function project_delete($request_data){
            $parameters = $request_data->get_params();
            global $wpdb;
            $result = $wpdb->delete('crm_projects',$parameters);
            if($result){
                //delete crm_costomers_projects table as well
                $result2 = $wpdb->delete('crm_customers_projects',$parameters);
                if(!$result2){
                    $response['code'] = "400";
                    $response['message'] = "crm_costomers_projects table excution fail ".$wpdb->show_errors();
                    $response['data'] = $parameters;
                    return $response;
                }
                $response['code'] = "200";
                $response['message'] = "Delete items has completed!";
                $response['data'] = $_empty;
                return $response;

            }else{
                $response['code'] = "400";
                $response['message'] = "crm_costomers table excution fail ".$wpdb->show_errors();
                $response['data'] = $request_data->get_params();
                return $response;
            }
        }

        static function history_events($request_data){
            $parameters = $request_data->get_params();
            $page_size = $parameters['page_size'];
            $page_num = $parameters['page_num'];
            if (!$page_num OR !$page_size){
                $response['code'] = "400";
                $response['message'] = "parameters can't be empty";
                $response['data'] = $_empty;
                return $response;
            }
            global $wpdb;
            $start_rowindex = ($page_num - 1)*$page_size;
            $sql = "SELECT * FROM crm_history ORDER BY event_time DESC LIMIT $start_rowindex, $page_size";
            $result_rows = $wpdb->get_results($sql, ARRAY_A);
            if (!$result_rows){
                $response['code'] = "400";
                $response['message'] = "Unception SQL errors";
                $response['data'] = $_empty;
                return $response;
            }
            $relevant_project=[];
            foreach($result_rows as $row){
                $str_temp = "";
                $_pre = "员工";
                if($row['subject_authority'] == 2){
                    $_pre ="管理员";
                }
                if($row['description']){
                    $str_temp = $_pre."".$row['subject']." ".$row['verb']." 客户 ".$row['object']." 到(从)项目 ".$row['description'];
                }else{
                    $str_temp = $_pre."".$row['subject']." ".$row['verb']." 客户 ".$row['object'];
                }
                $_temp = array("time" => $row['event_time'], "description" => $str_temp);
                array_push($relevant_project, $_temp);
            }
            $response['code'] = "200";
            $response['message'] = "return events list";
            $response['data'] = $relevant_project;
            return $response;
        }

        static function file_details($request_data){
            $parameters = $request_data->get_params();
            $project_id = $parameters['project_id'];
            $project_folder = __PLUGIN_ROOT__. "/" . "project". "/";
            if(! file_exists($project_folder)) mkdir($project_folder);
            $project_id_folder = $project_folder . $project_id . "/" ;
            if(! file_exists($project_id_folder)) mkdir($project_id_folder);
            $file_list =[];
            if ($handle = opendir($project_id_folder)) {
                while (false !== ($filename = readdir($handle))) {
                    if ($filename != "." && $filename != "..") array_push($file_list,$filename);
                }
                closedir($handle);
                $response['code'] = "200";
                $response['message'] = "Return file details lists!";
                $response['data'] = $file_list;
                return $response;
            }else{
                $response['code'] = "500";
                $response['message'] = "Sever internal errors!";
                $response['data'] = $_empty;
                return $response;
            }
            
        }

        static function file_upload($request_data){
            $parameters = $request_data->get_params();
            $project_id = $parameters['project_id'];
            $file = $_FILES['file'];
            $project_folder = __PLUGIN_ROOT__. "/" . "project". "/";
            if(! file_exists($project_folder)) mkdir($project_folder);
            $project_id_folder = $project_folder . $project_id . "/" ;
            if(! file_exists($project_id_folder)) mkdir($project_id_folder);
            $stored_url = $project_id_folder.$file['name'];
            if(move_uploaded_file($file['tmp_name'], $stored_url)){
                //generate url
                //update database crm_staff table
                //$url = "https://wordpress-202918-714756.cloudwaysapps.com/wp-content/plugins/api_plugin/staff/".$project_id."/".$file['name'];
                $response['code'] = "200";
                $response['message'] = "File upload successful!";
                $response['data'] = $_empty;
                return $response;
            }else{
                $response['code'] = "400";
                $response['message'] = "can't upload file, something wrong...";
                $response['data'] = $_empty;
                return $response;
            }     
        }

        static function file_delete($request_data){
            $parameters = $request_data->get_params();
            $project_id = $parameters['project_id'];
            $file_name = $parameters['file_name'];
            $project_folder = __PLUGIN_ROOT__. "/" . "project". "/";
            $project_id_folder = $project_folder . $project_id . "/" ;
            if( unlink($project_id_folder.$file_name)){
                $response['code'] = "200";
                $response['message'] = "File delete successful!";
                $response['data'] = $_empty;
                return $response;
            }else{
                $response['code'] = "500";
                $response['message'] = "can't delete this file, something wrong...";
                $response['data'] = $_empty;
                return $response;
            }
        }

        static function file_download($request_data){
            $parameters = $request_data->get_params();
            $project_id = $parameters['project_id'];
            $file_name = $parameters['file_name'];
            $url = "https://wordpress-202918-714756.cloudwaysapps.com/wp-content/plugins/api_plugin/project/".$project_id."/".$file_name;
            $response['code'] = "200";
            $response['message'] = "File download url generated!";
            $response['data'] = $url;
            return $response;
        }
        
        static function staff_avatar($request_data){
            $parameters = $request_data->get_params();
            $staff_id = $parameters['staff_id'];
            $file =  $_FILES['file'];
            $staff_folder = __PLUGIN_ROOT__ . "/" ."staff"."/" ;
            if(! file_exists($staff_folder)) mkdir($staff_folder);
            $staff_id_folder = $staff_folder .  $staff_id . "/";
            if(! file_exists($staff_id_folder)) mkdir($staff_id_folder);
            //delete the old avatar first.
            $p = scandir($staff_id_folder);
            foreach($p as $val){
                unlink($staff_id_folder.$val);
            }
            $stored_url = $staff_id_folder.$file['name'];
            //url https://wordpress-202918-714756.cloudwaysapps.com/wp-content/plugins/api_plugin/staff/1/laker.jpg
            if(move_uploaded_file($file['tmp_name'], $stored_url)){
                //generate url
                //update database crm_staff table
                $url = "https://wordpress-202918-714756.cloudwaysapps.com/wp-content/plugins/api_plugin/staff/".$staff_id."/".$file['name'];
                global $wpdb;
                $where['staff_id'] = $staff_id;
                $result = $wpdb->update('crm_staffs',array('staff_photo' => $url),$where);
                if(!$result){
                    $response['code'] = "400";
                    $response['message'] = "Unexception SQL error";
                    $response['data'] = $_empty;
                    return $response;
                }
                $response['code'] = "200";
                $response['message'] = "File upload successful!";
                $response['data'] = $_empty;
                return $response;
            }else{
                $response['code'] = "400";
                $response['message'] = "can' t upload file, something wrong...";
                $response['data'] = $_empty;
                return $response;
            }
        }

        static function password_change($request_data){
            $parameters = $request_data->get_params();
            $admin_id = $parameters['admin_id'];
            $staff_id = $parameters['staff_id'];
            $password = $parameters['new_password'];
            global $wpdb;
            $sql = "SELECT * FROM crm_staffs WHERE staff_id = '$admin_id'";
            $result = $wpdb->get_row($sql, ARRAY_A);
            if($result['staff_authority'] != 2){
                $response['code'] = "400";
                $response['message'] = "authority is not enough";
                $response['data'] = $_empty;
                return $response;
            }else{
                if($wpdb->update('crm_staffs',array('staff_password' => $password),array('staff_id' => $staff_id))){
                    $response['code'] = "200";
                    $response['message'] = "password update!";
                    $response['data'] = $_empty;
                    return $response;
                }else{
                    $response['code'] = "400";
                    $response['message'] = "Unexception SQL errors!";
                    $response['data'] = $_empty;
                    return $response;
                }                   
            }          
        }

        static function write_history($subject_id, $object_id, $verb, $project_list){
            global $wpdb;
            $sql = "SELECT * FROM crm_staffs WHERE staff_id ='$subject_id'";
            $result = $wpdb->get_row($sql, ARRAY_A);
            $sql2 = "SELECT * FROM crm_customers WHERE customer_id = '$object_id'";
            $result2 = $wpdb->get_row($sql2, ARRAY_A);
            $subject = $result['staff_name'];
            $subject_authority = $result['staff_authority'];
            $object = $result2['customer_name'];
            //branch
            if(! $project_list){
                $update_query = "INSERT INTO crm_history (event_time, subject, subject_authority, subject_id, object, object_id, verb)
                VALUES (NOW(),'$subject','$subject_authority','$subject_id','$object','$object_id','$verb')";
                if($wpdb->query($update_query)){
                    return TRUE;
                }
                return FALSE;
            }else{
                foreach($project_list as $p){
                    $sql3 = "SELECT * FROM crm_projects WHERE project_id = '$p'";
                    $result3 = $wpdb->get_row($sql3, ARRAY_A);
                    $description = $result3['project_name'];
                    $update_query = "INSERT INTO crm_history (event_time, subject, subject_authority, subject_id, object, object_id, verb, description)
                    VALUES (NOW(),'$subject','$subject_authority','$subject_id','$object','$object_id','$verb','$description')";
                    if(!$wpdb->query($update_query)){
                        return FALSE;
                    }
                }
                return TRUE;
            }    
        }


    }
}

?>