<?php
if ( ! defined( 'ABSPATH' ) ) {
	exit; // Exit if accessed directly.
}

if ( !class_exists( 'test' ) ) {
    class test
    {   
        function __construct(){

        }
		
		static function test_method(){
			$response["message"] = "POST的请求";
			$response["sent_data"] = $_POST;
			return $response;
		}
		
		static function test_method_t(){
			$response["message"] = "GET的请求";
			$response["sent_data"] = $_GET;
			return $response;
		}

    }
}

?>