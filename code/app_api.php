<?php
/*
Plugin Name: Api Plugin
Plugin URI:  
Description: Api Plugin
Version:     1.0
Author:      Leon Zhu
*/

if ( ! defined( 'ABSPATH' ) ) {
	exit; // Exit if accessed directly.
}

define('__PLUGIN_ROOT__', dirname(__FILE__)); 

foreach (glob( realpath(dirname(__FILE__)) .'/admin/*.php') as $filename){
	require_once $filename;
}

add_action( 'rest_api_init', function () {
	//接口访问网址：网站url + /wp-json/app_api/v1/test
	register_rest_route( 'app_api/v1', '/test/', array( 
					   'methods' => 'POST',
					   'callback' => 'test::test_method',
	) );
	//网站url + /wp-json/app_api/v2/test_two
	register_rest_route( 'app_api/v2', '/test_two/', array( 
					   'methods' => 'GET',
					   'callback' => 'test::test_method_t',
	) );
	/*代码注释 
	*/
	//网站url + /wp-json/app_api/v2/login/ 请求参数在body
	register_rest_route( 'app_api/v2', '/login/', array( 
						'methods' => 'POST',
						'callback' => 'myfunction::login',
	) );
    //网站url + /wp-json/app_api/v2/test_two/ 请求参数在body
	register_rest_route( 'app_api/v2', '/customers/', array( 
						'methods' => 'POST',
						'callback' => 'myfunction::customers',
	) );
    //网站url + /wp-json/app_api/v2/customers/details/ 请求参数在body
    register_rest_route( 'app_api/v2/customers', '/details/', array( 
						'methods' => 'POST',
						'callback' => 'myfunction::customer_details',
	) );
    //网站url + /wp-json/app_api/v2/customers/insert/ 请求参数在body
	register_rest_route( 'app_api/v2/customers', '/insert/', array( 
						'methods' => 'POST',
						'callback' => 'myfunction::customer_insert',
	) );

	register_rest_route( 'app_api/v2/customers', '/update/', array( 
						'methods' => 'POST',
						'callback' => 'myfunction::customer_update',
    ) );

    //网站url + /wp-json/app_api/v2/customers/insert/ 请求参数在body
	register_rest_route( 'app_api/v2/customers', '/delete/', array( 
						'methods' => 'POST',
						'callback' => 'myfunction::customer_delete',
    ) );
	
	// project 部分
	register_rest_route( 'app_api/v2', '/projects/', array( 
						'methods' => 'POST',
						'callback' => 'myfunction::projects',
	) );
	
	register_rest_route( 'app_api/v2/projects', '/details/', array( 
						'methods' => 'POST',
						'callback' => 'myfunction::projects_details',
	) );
	
	register_rest_route( 'app_api/v2/projects', '/insert/', array( 
						'methods' => 'POST',
						'callback' => 'myfunction::project_insert',
	) );
	
	register_rest_route( 'app_api/v2/projects', '/update/', array( 
						'methods' => 'POST',
						'callback' => 'myfunction::project_update',
	) );

	register_rest_route( 'app_api/v2/projects', '/delete/', array( 
						'methods' => 'POST',
						'callback' => 'myfunction::project_delete',
	) );

	register_rest_route( 'app_api/v2', '/historyevents/', array( 
		'methods' => 'POST',
		'callback' => 'myfunction::history_events',
	) );

	register_rest_route( 'app_api/v2/file', '/details/', array( 
		'methods' => 'POST',
		'callback' => 'myfunction::file_details',
	) );

	register_rest_route( 'app_api/v2/file', '/upload/', array( 
		'methods' => 'POST',
		'callback' => 'myfunction::file_upload',
	) );

	register_rest_route( 'app_api/v2/file', '/delete/', array( 
		'methods' => 'POST',
		'callback' => 'myfunction::file_delete',
	) );

	register_rest_route( 'app_api/v2/file', '/download/', array( 
		'methods' => 'POST',
		'callback' => 'myfunction::file_download',
	) );

	register_rest_route( 'app_api/v2/staff', '/avatar/', array( 
		'methods' => 'POST',
		'callback' => 'myfunction::staff_avatar',
	) );

	register_rest_route( 'app_api/v2/admin', '/password/', array( 
		'methods' => 'POST',
		'callback' => 'myfunction::password_change',
	) );


} );


?>